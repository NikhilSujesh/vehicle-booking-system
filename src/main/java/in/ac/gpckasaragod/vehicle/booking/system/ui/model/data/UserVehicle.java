/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.vehicle.booking.system.ui.model.data;

import java.util.Date;

/**
 *
 * @author student
 */
public class UserVehicle {
  private Integer id;
    private String userName;
     private String userGender;
    private Date userFrom;
    private Date userTo;
    private String userAddress;
    private String vehicleName;
    private String vehicleRegno;
    private Double vehicleRentperday;

    public UserVehicle(Integer id, String userName, String userGender, Date userFrom, Date userTo, String userAddress, String vehicleName, String vehicleRegno, Double vehicleRentperday) {
        this.id = id;
        this.userName = userName;
        this.userGender = userGender;
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.userAddress = userAddress;
        this.vehicleName = vehicleName;
        this.vehicleRegno = vehicleRegno;
        this.vehicleRentperday = vehicleRentperday;
    }

    
        
    

    public UserVehicle(Integer id, String userName, String userGender, Date userFrom, Date userTo, String userAddress, String vehicleName, String vehicleRegno, String vehicleRentperday) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public UserVehicle(Integer id, String userName, String userGender, String userFrom, String userTo, String userAddress, String vehicleName, String vehicleRegno, String vehicleRentperday) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }


    public String getUserGender() {
        return userGender;
    }

    public Date getUserFrom() {
        return userFrom;
    }

    public Date getUserTo() {
        return userTo;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public String getVehicleRegno() {
        return vehicleRegno;
    }

    public Double getVehicleRentperday() {
        return vehicleRentperday;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public void setUserFrom(Date userFrom) {
        this.userFrom = userFrom;
    }

    public void setUserTo(Date userTo) {
        this.userTo = userTo;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public void setVehicleRegno(String vehicleRegno) {
        this.vehicleRegno = vehicleRegno;
    }

    public void setVehicleRentperday(Double vehicleRentperday) {
        this.vehicleRentperday = vehicleRentperday;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    
}
