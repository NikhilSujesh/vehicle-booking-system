/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.vehicle.booking.system.service;

import java.util.List;
import in.ac.gpckasaragod.vehicle.booking.system.ui.model.data.Vehicle;
/**
 *
 * @author student
 */
public interface VehicleService {
    public String saveVehicle(String vehicleName,String vehicleType,String vehicleRegNo,String ACnonAC,Double vehicleRent,String vehicleFuelType);
    public Vehicle readVehicle(Integer ID);
    public List<Vehicle> getAllVehicles();
    public String updateVehicle(Integer id,String vehicleName,String vehicleType,String vehicleRegNo,String ACnonAC,Double vehicleRent,String vehicleFuelType);
    public String deleteVehicle(Integer id,String vehicleName,String vehicleType,String vehicleRegNo,String ACnonAC,Double vehicleRent,String vehicleFuelType);
    public String deleteVehicle(Integer ID);
}