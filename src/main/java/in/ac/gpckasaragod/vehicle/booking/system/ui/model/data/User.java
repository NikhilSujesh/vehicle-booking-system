/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.vehicle.booking.system.ui.model.data;

import java.util.Date;

/**
 *
 * @author student
 */
public class User {
    private Integer id;
    private Integer vehicleId;
    private String userName;
    private String userNumber;
    private String userGender;
    private Date userFrom;
    private Date userTo;
    private String userAddress;
   private String userRent;

    public User(Integer id, Integer vehicleId, String userName, String userNumber, String userGender, Date userFrom, Date userTo, String userAddress, String userRent) {
        this.id = id;
        this.vehicleId= vehicleId;
        this.userName = userName;
        this.userNumber = userNumber;
        this.userGender = userGender;
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.userAddress = userAddress;
        this.userRent = userRent;
        
    }
/*
    public User(Integer id, Integer vehicleId, String userName, String userNumber, String userGender, String userFrom, String userTo, String userAddress, String userTo0) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public User(Integer id, Integer vehicleId, String userName, String userNumber, String userGender, Date userFrom, Date userTo, String userAddress, Double userRent) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }*/

    public User(Integer id, Integer vehicleId, String userName, String userGender, String userNumber, String userFrom, String userTo, String userAddress, Double userRent) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public Integer getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public String getUserGender() {
        return userGender;
    }

    public Date getUserFrom() {
        return userFrom;
    }

    public Date getUserTo() {
        return userTo;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public String getUserRent() {
        return userRent;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public void setUserFrom(Date userFrom) {
        this.userFrom = userFrom;
    }

    public void setUserTo(Date userTo) {
        this.userTo = userTo;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public void setUserRent(String userRent) {
        this.userRent = userRent;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }
    
   
}
