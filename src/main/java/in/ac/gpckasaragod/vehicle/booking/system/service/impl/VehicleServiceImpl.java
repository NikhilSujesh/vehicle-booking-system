/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.vehicle.booking.system.service.impl;

import in.ac.gpckasaragod.vehicle.booking.system.service.VehicleService;
import in.ac.gpckasaragod.vehicle.booking.system.ui.model.data.Vehicle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class VehicleServiceImpl extends ConnectionServiceImpl implements VehicleService {

    @Override
    public String saveVehicle(String vehicleName, String vehicleType, String vehicleRegNo, String ACnonAC, Double vehicleRent, String vehicleFuelType) {
    try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO VEHICLE (NAME,TYPE,REGNO,RENTPD,ACNONAC,FUELTYP) VALUES "+ "('"+vehicleName+"','"+vehicleType+"','"+vehicleRegNo+"',"+vehicleRent+",'"+ACnonAC+"','"+vehicleFuelType+"')";
            System.err.println("Query:"+query);
            int status =  statement.executeUpdate(query);
            if(status != 1){
                return "save faild";
            }
            else{
                return "save successfull";
            }
        } catch (SQLException ex) {
            Logger.getLogger(VehicleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "save failed";
   }

    @Override
    public Vehicle readVehicle(Integer Id) {
         Vehicle vehicle = null;
        try {
            Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query="SELECT * FROM VEHICLE WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String vehicleName = resultSet.getString("NAME");
                String vehicleType = resultSet.getString("TYPE");
                String vehicleRegno = resultSet.getString("REGNO");
                Double vehicleRentperday = resultSet.getDouble("RENTPD");
                String vehicleAcnonac = resultSet.getString("ACNONAC");
                String vehicleFueltype = resultSet.getString("FUELTYP");
                vehicle = new Vehicle(Id, vehicleName, vehicleType, vehicleRegno, vehicleRentperday, vehicleAcnonac, vehicleFueltype);
                
                
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(VehicleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vehicle;
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        List<Vehicle> vehicles =new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM VEHICLE";
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String vehicleName = resultSet.getString("NAME");
                String vehicleType = resultSet.getString("TYPE");
                String vehicleRegno = resultSet.getString("REGNO");
                Double vehicleRentperday = resultSet.getDouble("RENTPD");
                String vehicleAcnonac = resultSet.getString("ACNONAC");
                String vehicleFueltype = resultSet.getString("FUELTYP");
                Vehicle vehicle = new Vehicle(id, vehicleName, vehicleType, vehicleRegno, vehicleRentperday, vehicleAcnonac, vehicleFueltype);
                vehicles.add(vehicle);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(VehicleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vehicles;
    }

    @Override
    public String updateVehicle(Integer id, String vehicleName, String vehicleType, String vehicleRegNo, String ACnonAC, Double vehicleRent, String vehicleFuelType) {
        try {
            Connection connection=getConnection();
            String query="UPDATE VEHICLE SET NAME=?,TYPE=?,REGNO=?,RENTPD=?,ACNONAC=?,FUELTYP=? WHERE ID=?";
            PreparedStatement statement=connection.prepareStatement(query);
            statement.setInt(7, id);
            statement.setString(1, vehicleName);
            statement.setString(2, vehicleType);
            statement.setString(3, vehicleRegNo);
            statement.setDouble(4, vehicleRent);
            statement.setString(5, ACnonAC);
            statement.setString(6, vehicleFuelType);
            int update=statement.executeUpdate();
            if (update!=1)
                return "Update failed";
                        else
                return"update successfull";
                
        } catch (SQLException ex) {
            Logger.getLogger(VehicleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
     return "Update failed";   
    }

    @Override
    public String deleteVehicle(Integer id, String vehicleName, String vehicleType, String vehicleRegNo, String ACnonAC, Double vehicleRent, String vehicleFuelType) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String deleteVehicle(Integer ID) {
        try {
            Connection connection =getConnection();
            String query="DELETE FROM VEHICLE WHERE ID=?";
            PreparedStatement statement=connection.prepareStatement(query);
            statement.setInt(1, ID);
            int delete = statement.executeUpdate();
            if(delete ==1){
                return "delete successfull";
            } else {
                return "delete failed";
            }
        } catch (SQLException ex) {
            Logger.getLogger(VehicleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
      return "delete successfull";
    }

}
