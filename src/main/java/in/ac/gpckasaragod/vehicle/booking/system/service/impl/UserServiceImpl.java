 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.vehicle.booking.system.service.impl;

import in.ac.gpckasaragod.vehicle.booking.system.service.UserService;
import in.ac.gpckasaragod.vehicle.booking.system.ui.model.data.User;
import in.ac.gpckasaragod.vehicle.booking.system.ui.model.data.UserVehicle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class UserServiceImpl extends ConnectionServiceImpl implements UserService{

    @Override
    public String saveUser(String userName, String userGender, String userPhone, String userAddress, Date userFrom, Date userTo, Double userRent, Integer vehicle) {
    try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String formatedDate1 = dbDateFormat.format(userFrom);
            String formatedDate2 = dbDateFormat.format(userTo);
            String query = "INSERT INTO USER (NAME,GENDER,PHONE,ADDRESS,FRO_M,T_O,RENT,VEHICLEID) VALUES "+ "('"+userName+"','"+userGender+"','"+userPhone+"','"+userAddress+"','"+formatedDate1+"','"+formatedDate2+"',"+userRent+",'"+vehicle+"')";
            System.err.println("Query:"+query);
            int status =  statement.executeUpdate(query);
            if(status != 1){
                return "save faild";
            }
            else{
                return "save successfull";
            }
        } catch (SQLException ex) {
            Logger.getLogger(VehicleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "save failed";
   } 
    @Override
    public User readUser(Integer id) {
        User user =null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query="SELECT * FROM USER WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String userName = resultSet.getString("NAME");
                String userGender = resultSet.getString("GENDER");
                String userNumber = resultSet.getString("PHONE");
                String userAddress = resultSet.getString("ADDRESS");
                String userFrom = resultSet.getString("FRO_M");
                String userTo = resultSet.getString("T_O");
                Double userRent = resultSet.getDouble("RENT");
                Integer vehicleId = resultSet.getInt("VEHICLEID");
                user = new User(id, vehicleId, userName, userGender, userNumber, userFrom, userTo, userAddress, userRent);
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return user;
    }

    
    @Override
    public List<UserVehicle> getAllUserVehicleDetails() {
        List<UserVehicle> uservehicle = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT USER.ID,USER.NAME,USER.GENDER,USER.ADDRESS,USER.FRO_M,USER.T_O,VEHICLE.NAME,VEHICLE.REGNO,VEHICLE.RENTPD FROM VEHICLE JOIN USER ON VEHICLE.ID = USER.VEHICLEID";
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("USER.ID");
                String userName = resultSet.getString("USER.NAME");
                String userGender = resultSet.getString("USER.GENDER");
                String userAddress = resultSet.getString("USER.ADDRESS");
                Date userFrom = resultSet.getDate("USER.FRO_M");
                Date userTo = resultSet.getDate("USER.T_O");
                String vehicleName = resultSet.getString("VEHICLE.NAME");
                String vehicleRegno = resultSet.getString("VEHICLE.REGNO");
                Double vehicleRentperday = resultSet.getDouble("VEHICLE.RENTPD");
                UserVehicle userVehicle = new UserVehicle(id, userName, userGender, userFrom, userTo, userAddress, vehicleName, vehicleRegno, vehicleRentperday);
                uservehicle.add(userVehicle);
                
            
            }
              
        } catch (SQLException ex) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return uservehicle;
    }

    @Override
    public String updateUser(Integer id, String userName, String userGender, String userPhone, String userAddress, Date userFrom, Date userTo, Double userRent, Integer vehicle) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyy-MM-dd");
            String formatedDate1 = dbDateFormat.format(userFrom);
            String formatedDate2 = dbDateFormat.format(userTo);
            
            String query="UPDATE USER SET NAME='"+userName+"',GENDER='"+userGender+"',PHONE='"+userPhone+"',ADDRESS='"+userAddress+"',FRO_M='"+formatedDate1+"',T_O='"+formatedDate2+"',RENT="+userRent+" VEHICLEID="+vehicle+" WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
                return "update failed";
            else
                return "update successfull";
        } catch (SQLException ex) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "update";
        
        
         }

    @Override
    public String deleteUser(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM USER WHERE ID =?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "failed";
            else 
                return "delete successfull";
        } catch (SQLException ex) {
            Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "failed";
            
    }

   
    }

    
    

    
    

