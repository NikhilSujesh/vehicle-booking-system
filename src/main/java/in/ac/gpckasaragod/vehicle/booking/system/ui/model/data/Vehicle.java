/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.vehicle.booking.system.ui.model.data;

/**
 *
 * @author student
 */
public class Vehicle {
   private Integer id;
    private String vehicleName;
    private String vehicleType;
    private String vehicleRegno;
    private Double vehicleRentperday;
    private String vehicleAcnonac;
    private String vehicleFueltype;

    public Vehicle(Integer id, String vehicleName, String vehicleType, String vehicleRegno, Double vehicleRentperday, String vehicleAcnonac, String vehicleFueltype) {
        this.id = id;
        this.vehicleName = vehicleName;
        this.vehicleType = vehicleType;
        this.vehicleRegno = vehicleRegno;
        this.vehicleRentperday = vehicleRentperday;
        this.vehicleAcnonac = vehicleAcnonac;
        this.vehicleFueltype = vehicleFueltype;
    }

    public Integer getId() {
        return id;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getVehicleRegno() {
        return vehicleRegno;
    }

    public Double getVehicleRentperday() {
        return vehicleRentperday;
    }

    public String getVehicleAcnonac() {
        return vehicleAcnonac;
    }

    public String getVehicleFueltype() {
        return vehicleFueltype;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public void setVehicleRegno(String vehicleRegno) {
        this.vehicleRegno = vehicleRegno;
    }

    public void setVehicleRentperday(Double vehicleRentperday) {
        this.vehicleRentperday = vehicleRentperday;
    }

    public void setVehicleAcnonac(String vehicleAcnonac) {
        this.vehicleAcnonac = vehicleAcnonac;
    }

    public void setVehicleFueltype(String vehicleFueltype) {
        this.vehicleFueltype = vehicleFueltype;
    }

    @Override
    public String toString() {
        return "Name : "  + vehicleName + " RegNo :  " + vehicleRegno + "";
    }
    
}
