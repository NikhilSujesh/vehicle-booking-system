/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.vehicle.booking.system.service;

import in.ac.gpckasaragod.vehicle.booking.system.ui.model.data.User;
import in.ac.gpckasaragod.vehicle.booking.system.ui.model.data.UserVehicle;
import java.util.Date;
import java.util.List;

/**
 *
 * @author student
 */
public interface UserService {
    public String saveUser(String userName,String userGender,String userPhone,String userAddress,Date userFrom,Date userTo,Double userRent,Integer vehicle);
    public User readUser(Integer id); 
    public List<UserVehicle>getAllUserVehicleDetails();
    public String updateUser(Integer id,String userName,String userGender,String userPhone,String userAddress,Date userFrom,Date userTo,Double userRent,Integer vehicle);
    public String deleteUser(Integer id);
}